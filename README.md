# DDD-Service-Catalog-Templates

Domain Directory Structure Template.

```mermaid
graph TD
A(Domain)
A -->B(subdomain)
B --> C(DDD-Service-Catalog-Template)
C --> F(src)
C --> D(model_artifacts)
C --> E(published-language)
C --> G(ubiquitous-language)
D --> J(context-map)
D --> K(data-model)
D --> L(misc)
D --> M(services.yml)
```



Template Structure guidelines:
-------------------------------


1. DDD-Service-Catalog-Template Repository must have below directory and file:

~~~
 ubiquitous-language/
 published-language/ 
 model-artifacts/ 
 src/
 <subdomain>-HLA.docx file
 <subdomain>-LLD.docx file
~~~


2. ubiquitous-language directory must have glossary file with csv extension:

~~~
 <subdomain-name>-glossary.csv
~~~

3. published-language directory must have YAML file:

~~~
 <file-name>.yml
~~~

4. model-artifacts should have atleast data-model as a child directory amongs the below:

~~~
 context-map/ 
 data-model/
 misc
~~~

5. data-model directory must have events.md markdown file:

~~~
 <subdomain-name>-events.md
~~~

Configuring hook script:
-------------------------

### To run the hook script
1. copy the code from lucid-push-rules.sh and paste the code under .git/hooks/prepare-commit-msg.sample
2. Once copied in prepare-commit-msg.sample then rename the file to `prepare-commit-msg`
3. provide executable access to prepare-commit-msg file using `chmod a+x <path/to/prepare-commit-msg>`
4. initilize the git again using `git init` this will link the current hook script to git
5. Now hooks are integrated once anything is updated/modified and we try to push the changes, then this hook script will get executed at `git commit` command level
